import { LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

/**
     * Clase API que servira como provider para consumir los servicios rest.
*/
export class API {

    // variable que se empleara para agregarla a la url del servicio.
    public ip: any = "http://appservicios.ingeneo.co/api/";

    constructor(public http: Http,
        public loadingCtrl: LoadingController) {
        console.log('Hello API Provider');
    }


    // servicios para las ciudades
    getTest() {
        // si no hay paises paso a consultar la lista de paises y a almacenarlos localmente
        let url = `${this.ip}/test/getTest`;
        return this.http.get(url).map(res => {
            return res.json();
        }, error => {
            return error.json();
        })
    }
}
