import { API } from './services';
import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [AndroidFingerprintAuth, API],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateY(50%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateY(0%)'
        }))
      ])
    ]),
    trigger('scaleEfect', [
      state('in', style({ transform: 'scale(1)' })),
      transition('void => *', [
        style({
          transform: 'scale(0)'
        }),
        animate('0.6s ease-in')
      ]),
      transition('* => void', [
        animate('0.6s ease-out', style({
          transform: 'scale(1)'
        }))
      ])
    ])
  ]
})
export class LoginPage {

  // creación de controlador para el formulario
  public login_form: FormGroup;
  // variables para almacenar el correo electronico y la contraseña
  public email: string = '';
  public pwd: string = '';
  constructor(public navCtrl: NavController,
    private API:API,
    private androidFingerprintAuth: AndroidFingerprintAuth,
    private formBuilder: FormBuilder,
    public navParams: NavParams) {
    this.login_form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.minLength(4), Validators.required])],
      pwd: ['', Validators.compose([Validators.minLength(3), Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  // función para verificar si un usuario ya existe en el sistema
  verifyUser(data, type) {
  }

  goToView() {

  }

  loginWithFinger() {
    this.androidFingerprintAuth.isAvailable()
      .then((result) => {
        if (result.isAvailable) {
          // it is available

          this.androidFingerprintAuth.encrypt({ clientId: 'myAppName', username: 'myUsername', password: 'myPassword' })
            .then(result => {
              if (result.withFingerprint) {
                console.log('Successfully encrypted credentials.');
                console.log('Encrypted credentials: ' + result.token);
              } else if (result.withBackup) {
                console.log('Successfully authenticated with backup password!');
              } else console.log('Didn\'t authenticate!');
            })
            .catch(error => {
              if (error === this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
                console.log('Fingerprint authentication cancelled');
              } else console.error(error)
            });

        } else {
          // fingerprint auth isn't available
        }
      })
      .catch(error => console.error(error));
  }

}
