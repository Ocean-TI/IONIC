import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';

/**
 * Generated class for the InitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-init',
  templateUrl: 'init.html',
})
export class InitPage {
  // tab por defecto para la vista init
  public seltabix: any = 0;
  // variable donde se guarda la ruta indicada desde el menu
  public directionView: any;
  pages: Array<{ title: string, component: any, icon: string, root: number, badge?}>;
  // funcionalidad necesaria para las tabs
  // variable que indica cual tabs sera desplegada desde otra vista
  public root: 'LoginPage';
  @ViewChild("myTabs") tabs: Tabs;
  constructor(public navCtrl: NavController,
    public zone: NgZone,
    public navParams: NavParams) {
    this.pages = [
      { title: 'Iniciar sesión', component: 'LoginPage', icon: 'log-in', root: 0 },
      { title: 'Registrarme', component: 'RegistryPage', icon: 'contact', root: 1 }
    ];
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad InitPage');
  }

  applyRoot(root) {
    console.log("cambio")
    this.zone.run(() => {
      this.seltabix = root;
    });
  }
}
