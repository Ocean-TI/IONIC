import { USER } from './../../providers/config/config';
import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController } from 'ionic-angular';
/**
 * Generated class for the RegistryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registry',
  templateUrl: 'registry.html',
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateY(50%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateY(0%)'
        }))
      ])
    ]),
    trigger('scaleEfect', [
      state('in', style({ transform: 'scale(1)' })),
      transition('void => *', [
        style({
          transform: 'scale(0)'
        }),
        animate('0.6s ease-in')
      ]),
      transition('* => void', [
        animate('0.6s ease-out', style({
          transform: 'scale(1)'
        }))
      ])
    ])
  ]
})
export class RegistryPage {
  // creación de contoler para el formulario de registro
  public register_form: FormGroup;
  // formulario principal
  public forms = <USER>{};
  // formulario para red social
  public forms_social = <USER>{};


  constructor(public modalCtrl: ModalController,
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    public navParams: NavParams) {
    this.createForm();
  }

  getCountry() {
    let modal = this.modalCtrl.create('CountryPage');
    modal.onDidDismiss(data => {
      console.log(data);
    });
    modal.present();
  }

  ionViewDidLoad() {
    this.getCountry();
    console.log('ionViewDidLoad RegistryPage');
  }
  createForm() {
    // validaciónes para el formulario
    this.register_form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      pwd: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      phone: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.required])],
      city: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      u_names: ['', Validators.compose([Validators.minLength(4), Validators.required])],
      u_lastnames: ['', Validators.compose([Validators.minLength(4), Validators.required])],
    });

    // asign data on form
    this.register_form.setValue({
      email: '',
      pwd: '',
      phone: '',
      city: '',
      u_names: '',
      u_lastnames: '',
    });
  }
  register(data) {
  }
}
