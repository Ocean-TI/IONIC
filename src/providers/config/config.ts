// estructura de los datos del usuario
export interface USER {
  role: number,
  name_user: string,
  pwd: string,
  email: string,
  phone: string,
  city: string,
  name: string,
  gender: string,
  u_lastnames: string,
  user_pic: string,
  u_names: string,
  country_name: string,
  token: string,
}